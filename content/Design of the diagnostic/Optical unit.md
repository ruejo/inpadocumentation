The optical units transmits the light from the scintillator to the INPA cameras and the PMTs fibers. 
The optical design was done by: [Optical Development](https://opticaldevelopment.es/)

The conceptual design carried out by Optical Development can be found in this [file](../assets/INPA_Optical_Preliminary_Design.docx)

A detailed description of it performance in terms of focus and distortion ca be found in [J. García-Dominguez et al. 2022](https://doi.org/10.1109/TPS.2022.3201495)

As a reference which may come handy, the list of lenses and mirrors of the system is given below:

| Item          | Radius / Dimensions (mm) | Focal distance | Surface deviation | Coating                     |
| ------------- | ------------------------ | -------------- | ----------------- | --------------------------- |
| Prism         | 6                        | -              | $\lambda/4$       | MgF2 and Enhanced Aluminium |
| Lens - 0      | 25                       | 35             |                   | YAG-BBAR                    |
| Lens - 1      | 46                       | 100            |                   | MgF2                        |
| Lens - 2      | 46                       | 400            |                   | MgF2                        |
| Lens - 3      | 50.8                     | 400            |                   | MgF2                        |
| Lens - 4      | 50.8                     | 800            |                   | MgF2                        |
| Lens - 5      | 76.2                     | 250            |                   | MgF2                        |
| Mirror - 0    | 40x40x8                  |                | $<\lambda/4$      | Enhanced Aluminium          |
| Mirror - 1    | 60x45x5                  |                | $<\lambda/4$      | Enhanced Aluminium          |
| Mirror - 2    | 60x45x5                  |                | $<\lambda/4$      | Enhanced Aluminium          |
| Beam splitter | 50x7x3                   |                |                   | Enhanced Aluminium          |
| @ToDo         | Add PMTs doublet data    |                |                   |                             |

