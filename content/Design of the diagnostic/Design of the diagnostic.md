---
title: Design of the diagnostic
draft: false
Category: Design
---
The mechanical system was design by [Javier García Dominguez](https://orcid.org/0000-0002-3903-0560) with the inputs of the *Physics design* done by [Jose Rueda Rueda](https://orcid.org/0000-0002-4535-326X) and the expertise and advise given by the IPP team. Details of the thermal and magnetic assessment of the INPA design can be found on this publication: [J. García-Dominguez et al. 2022](https://doi.org/10.1109/TPS.2022.3201495)

There are three main elements of the INPA mechanical design
1. [Diagnostic head](Diagnostic%20head.md)
2. [Optical unit](Optical%20unit.md)
3. [Out-vessel system](Out-vessel%20system.md)