---
title: Diagnostic head
draft: false
Category: Design
---
The diagnostic head is the main mechanical component of the INPA diagnostic. It contains the scintillator, carbon foil, magnetic shutter and the first optical elements.
The brief overview is given in the figure below

![CAD overview of the diagnostic head](../assets/image-20240325153216644.png)

The details of the frontal plate, which contains the scintillator and collimator can be seen in the figure below

![Frontal plate of the INPA](../assets/image-20240325153305929.png)




