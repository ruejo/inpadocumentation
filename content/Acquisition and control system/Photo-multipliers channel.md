---
title: Photo-multipliers channel 
draft: false
Category: Index
---
# General idea of the installation

The fast channel of the INPA is based on PMTs, as the FILD 1 setup. The idea is: light from the INPA -> Light fibers to control room 2 -> ADC and computer card -> data fibers to control room 1 -> Acquisition computer

The PTMs and ADC are located in the cabinet 116 from control room 2

# Fibers installation
## Datasheet information
Fibers are from company Laser Components Model HCP-M0800T OPTICAL FIBER LOW LOSS, 800/830/1040um. The diameter thickness is 800 $\mu$m of silica glass plus 200 $\mu$m from insulating, around 1 mm in total. They are the very same fibers used for FILD 2. Some data in the table below:

| Parameters                         | Values                |
| ---------------------------------- | ---------------- |
| Silica diameter                    | 800 microns      |
| Total diameter                     | 1040 microns     |
| Maximum short term bending  radius | 73 mm            |
| Maximum long term bending radius   | 118 mm           |
| Cost                               | 8.45 euros /m    |
| Attenuation                        | @850nm </=8dB/km |
| water content                      | low OH           |
| Shilding materials                 | HCS,ETFE         |

Note, the numerical apperture is 0.37, so light should come with an angle below 22 degrees:
![Relation between NA and covered angle, taken from FILD1 design notes](../assets/Pasted_image_20231004161823.png)

## Fiber head installation and routing
The fibers were fixed in a metal head, in a compact circular arragement done by Michael Rolfs. The total length is of 90 m from port to PMTs, but a spare couple of meter was added, in order to be able to change the head if needed.


The Initial design of the arrange of fibers was
![[../assets/Pasted_image_20231004154347.png|Initial CAD design of the fibers arrangement]]

Final aspect of the arrangement
![Final slice of the INPA fibers head](../assets/Pasted_image_20231004154927.png)

This is to be installed in a metal tube to be easy to handle
![Drawing of the metal tube supporting the fibers bundle](../assets/Pasted_image_20231004154818.png)

For comparison, FILD1 fiber design:
![FILD fiber bundle design](../assets/Pasted_image_20231004161650.png)
## Fibers connection to the PMTS
The coupling between the fibers and the PMTs is direct: fiber is placed just in-front of the PMT, touching the PMT surface, as can be seen below. The metal support allows for adjusting the vertical and horizontal position of the PMT array with high precision:
![PTM array and fibers inside the control room box](../assets/IMG_1168.jpg)

# PMT
## Datasheet information

The PMTs are the same of the ones used for FILD1, Hammamatsu H9530-20. This is an array of 8 channels in a single PMT module, photo below. The 8 channels share the same power supply input, and have a separate output. The output is a single pin, for the installation, this is connected to a BNC connector in order to route the signal to the ADC. 

>Note, there is a whole family of PMTs arrays H9530-XX, each having a different peak sensitivity, ie, if we change the scintillator material and the emitted light has a different wavelength, we would need to change the PMT

| Property                          | Value       |
| ---------------------------- | --------- |
| Cross talk between channels  | few percent       |
| HV input                     | 1000 V    |
| Current output               | Up to 1mA |

## Cable Connection
![ Pin Order of the PMT channels](../assets/Pasted_image_20231004154030.png)

## ADC
The ADC take as input the current signal from the PMT and produce a digital signal directed towards the PC. The ADC cards were done by IPP. Each one has capacity for 2 channels.

The signal from all channels is collected by a module and send it to the adquisition PC, located in control room 1

# Calibration of the PMTs and shotfile
## Calibration procedure

In order to calibrate the ADCs, a small power supply is connected to each ADC, as it would be the PMT emitting a signal
## Digital Oscilloscope
There is a digital oscilloscope which allows to to monitor in life the signal registered by the SiO2 module. To open it, connect to the acquisition PC and execute `SciOszi` :
```Bash
ssh NPI
SioOszi
```
the following window will pop-up:

![SciOzci main window](../assets/GetImage.jpeg)

Click in `Aktionen`and then in `open` 
You will get a small list to select the desired SiO card connected to the computer, for the INPA, select `Dev/SiO2` Now click on `DMA` and in `DMA Aurchfuehren` this will open the following table:
![ Parameters of the digital oscilloscope](../assets/GetImage_(1).jpeg)

There you have:
- Samplebstand (ns):  the sampling rate for the oscilloscope
- Aufnahmedauer (s): the refresh time, after that number of seconds, the data on the screen is erased
- AktiveKanaele an Link 0-4: The number of canals connected to each link card
- Monitor Kanal: Channel to plot
- Obere/Untere Schranke: minimum and maximum limits for the y axis

After clicking on `START`you will get something like:
![Example of Oscilloscope measurement](../assets/GetImage_(2).jpeg)

The green line is the current measurement period (Aufnahmedauer) while the blue one the previous one

## Channel testing
In order to test the channel, Wolfgang put together a small device. Basically a portable power supply connected to a 1.7 k$\Omega$ resistor. Such that we get the signals up to 1mA we want


![Power supply and multimeter](../assets/GetImage_(3)_1.jpeg)
![Voltage set in the Power  Supply](../assets/GetImage_(4).jpeg)

## Channel calibration
In the 2020 Campaing, we used the calibration parameters from FHC (FILD1). These produce a signal in volts, but we need the values in mA to relate with the number of photon reaching the PMT and hence the number of impinging ions on the scintillator.

In the case of linear calibrations, the output signal when reading a shotfile is: $S_j = a_jS_0 + b_j$ where $S_0$ is the raw signal in 'shotfile units' and $a_j,b_j$ the calibration parameters stored in the shotfile. The calibration is applied $n$ times, each time for each set of $j$ constant we have. Notice that this is equivalent to do: $S = c S_0 + B$ with $C = \prod a_j$ and $B = \sum_j^{n}a_j b_{j-1} + b_n$ . But the AUG system is implemented in this series of multistage linear calibrations because you can save in $j=1$ the original calibration and in successive $j$ the minor adjustments for each campaings

In any case, the parameters in FHC has $b_j = 0$ and 3 $a_j$ so to obtain my raw signal I just need: $S_{RAW} = S_{Calibrated}/(a_0a_1a_2)$ 

Now, as we want the future calibration signal to be directly the current, we just need $Current = a_0' S_{RAW} + B_0'$. So just take 2 measurements with the power supply at 2 currents, average over some seconds to avoid noise and done. 

It turns our that for all the channels, the difference in the constant $a_0$ is less than 1 per mill among them, so we can set all the channels to have the same value. Fot $b_j$ not, as there is like a 10% variation among channels, so we need to implement a offset correction different for each channels, as expected. It is also expected this small offset to change among campaigns.

The average for all channels is:
$\langle a_0 \rangle = (1.670 \pm 0.004)\times 10^{-4} mA/Count$
$\langle b_0 \rangle= -0.013 \pm 0.01 mA$
The constant for each channel would be (execute the axuliary_script number 3 of the INPA documentation to get these plots and fits):




![Top inverse of the calibration factor a0, bottom -b0/a0](../assets/GetImage_(5).jpeg)

