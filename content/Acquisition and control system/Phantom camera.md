---
title: Phantom camera
draft: false
Category: Index
---

# Overview

This manual contain the instruction to operate the INPA/FILD phantom cameras. The network configuration is just placed for future reference, you can skip this part just for the operation. If you want information of how to install the phantom camera, please see the section [Installation of the diagnostic](../Installation%20of%20the%20diagnostic/Installation%20of%20the%20diagnostic.md)

The camera manual is found in this link: [PDF](../assets/v2512.pdf)

The complete manual of the PCC software can be found here: [PDF](../assets/PCC.pdf)

# General view

The phantom cameras are controlled via a new (2022) computer (`WXFITD69`). Up to now, just the commercial program is used. This force the user to interact quite a lot with the software, which is always a source of problems, solutions for a more automated installation are being investigated.

This computer allows just for a single session / user. So if you connect, you will stole the connection from the other user.

> [!important]
> Communicate to Moritz Gruber (#1319) that you will need to use the PC before using it



# Network setting

Since 2022 campaign, all phantom cameras must be connected to the new phantom network (if not, they will not be found by the `WXFITD69` computer). So all IP addressed should start with: 172.16…

This can be easy changed/checked with the commercial software of the company (just connect the camera to a laptop) M. Gruber have one with all necessary software in the CR, you can borrow it (after asking permission) if you need it.

Step by step instructions of how to change this IP can be found in page 9 of the camera manual (see link above).

**Note on 1 Gb connection:** old cameras which have just 1Gb connection, can be routed to the 172.16 network just doing this IP trick. New cameras, which has a 10Gb option, have the 10Gb network adapter on the 172.16… address, and this default can’t be changed. This cause issues if you intent to operate via the 1Gb connection, as you cannot set the 1Gb IP to the 172.16… without entering in conflict with the 10Gb one and you can’t find the camera in `WXFITD69` if you are not in the 172.16 networks so:

> [!warning]
> New Phantoms cameras can only be operated in the 10 Gb network


# How to turn on the camera

To turn on the camera, you just need to plug it: There is no remote controlled plug (@ March 2024): you need to plug it yourself. Enter to the torus hall and go to Sector 16. Behind the SPI there is a transformer fixated with some yellow tape where is written INPA, just plug it in the plug behind (in a column near by). Sorry, photo is missing, to be included in the next version

# How to connect the camera to the network

The FILD camera only have a 1Gb connection, so just plug the camera to one of the free optical insulator in the network switch near the SPI

In the case of the INPA, the 10Gb port is temporally shared with the SPI team. Before operation, one must connect the 10 Gb cable in the port of the network switch. (pictures will be included in the new version). You must remove the yellow cable connected in the right most port of the network switch (the one near the optical fibers) and plug the INPA Ethernet cable. 

> [!important]
> Coordinate with Moritz and the SPI team before doing this!

# How to connect to the computer

If order to connect to the computer, just type in a terminal:

```bash
xfreerdp /u:vida /d:ipp-ad /size:85% /v:wxfitd69
```

Note that:

- `/u`: means user, `vida` is the username to use (you should not connect with your user to this pc, as you will have nothing configured to operate the camera)
- `/size`: is the size of the windows to be opened, relative to your current window size

you will be asked the password, not included here for safety reason. Ask Moritz for it

> [!tip]
> Tip: You can add this to your startup file with an alias so you don’t need to write the whole line each time

# Recording instructions

With the camera connected, connect to the `wxfitd69` computer and launch the PCC program (there is a shortcut in the desktop). On the right, you will see a tabbed menu:

- Live: Allows you to set the recording parameters and start the recording
- Play: Allows you to see the recorded video
- Manager: Allows you, among others, to change IP and network settings of the camera

You do not need to change any camera settings during operation, just the option located in the ‘Cine settings’ section of the ‘Live’ tab are useful for the operation.

> [!disclaimer]
> Disclaimer: During the 2022 campaign, the 10Gb network was kinda slow to recognize the camera, usually, one connect the camera and like 25 minutes after, the computer recognize it. Once the recognition take place, the camera is operated smoothly without lag

### Camera selection

At the beginning of the Live tab, there is an unfoldable list you where you must select the camera. Each camera is identified by the ID number example:

- 9404: FILD1 camera
- 24167: INPA camera
![Screen shot with the camera list](../assets/Pasted_image_20230925130028.png)


### Settings: long explanation

The menu of the Cine Settings (`.cine` is the name of the video format used by the phantom, this is why this menu is called like this), in the Live tab, contain all camera parameter to be considered. (see image below)

![Detailed view of the Settings pannel](../assets/Pasted_image_20230925130626.png)

In this Menu (in **bold** the parameters you need to pay attention to, a simplified version just focusing in the few options you must check is written in the next section) as said in the software manual:

- Cine - activates when 'Camera Settings > Partitions' is set to a value greater than 1. By default 'Preview' mode is selected to provide a live image to the computer, and the ability to set recording parameters that can be applied to all partitions. The field automatically changes to the first available (unused) partition 'Cine (segment) Number' when the camera is placed into record mode. There is no need to change this during operation
    
- Name - used to assign a name to the Cine stored in the camera's RAM, and is written to the camera Cine structure meta-data. A counter will be added at the endof this name for each recorded cine. This name can be viewed in the 'Play > Cine Info' selector. Again, there is no need to change this during operation
    
- Description - enter up to 4,096 characters (including spaces and special characters) to describe the current Cine. This description is applied to all partitions. It is included in the Cine meta-data, however it cannot be edited after the Cine is captured
    
- **Resolution** - set the dimensions (width x height) of the images (camera dependent). Notice that seting this to XXxYY does not mean the sensor is rescaled into XX times YY pixels, but just only XX times YY pixels will be read. The middel pixel of the selected range will always be the center of the sensor. For FILD1, you must set the resolution to 800x600, if not, you won’t be able to record the whole shot (lack of memory) for INPA, you can leave the whole resolution.
    
- **Sample Rate** - sets the acquisition frame rate in frames-per-second (fps). By default, you can use 1000 fps for FILD1 and 100 fps for INPA
    
- **Exposure Time** - sets the exposure time, which is the length of time the sensor is exposed to light per frame. This is represented in microseconds (μs). By default the best is to write a random large number and click enter. The program will take the larger possible exposure time (compatible with your sample rate). It will be always a bit smaller than 1/SampleRate, as the camera need time to read the frame
    
- EDR (Extreme Dynamic Range) - applies a secondary exposure to pixels that become saturated or overexposed. Do not use this!! Using this make that each pixel have different exposure times to the linearity is broken.
    
- Exposure Index - sets the exposure index (Effective ISO) of the image, by loading preset tone curves to increase the effective ISO of the camera up to 5x the base value. This only affect the color information, not the raw data, so you can ignore this
    
- **CSR** - (Current Session Reference) - performs a black reference that calibrates the image for current 'Cine Settings' parameters. You need to push this button always before the recording.
    
- Low Light - available in Preview/Idle mode only (not Capture). Temporarily reduces the 'Sample Rate' and increases the 'Exposure Time' to a pre-defined level based on the 'Auto Exposure' settings. This feature can be useful for setting up in low light conditions where illumination will be provided at the time of the event.
    
- Close Shutter - closes the internal / external shutter (if applicable) for the selected camera, and any camera assigned to its associated group (if applicable)
    
   ![ ](../assets/Pasted_image_20230925130716.png)
    
- **Image Range & Trigger Position** - is a double-cursor slider bar that represents the memory buffer of the camera, and the amount of memory used to store 'Pre-trigger' frames (red), Post-trigger' frames (green), or a user-specified 'Trigger Delay' (white).
    
    - Last (pull-down selection list /entry field) - used to set the 'Post-trigger' or 'Image Range (delay) value from a list of common values or a user-specified value.
    - T (Trigger Position slider) - sets the number of image / frames to be saved after a trigger has been detected, and automatically changes the value in the 'Last' entry box. Since the camera memory is a FIFO (First-In First-Out) circular memory buffer, the Post Trigger value also sets the reciprocal number of 'Pre-trigger' frames captured.
        - To determine the value to enter:
            - 1. Post-trigger = Sample Rate x Required 'post-trigger' duration (in seconds)
            - 2. Pre-trigger = (Sample Rate x Required 'post-trigger' duration (in seconds)) - Duration (below slider bar)
            - 3. Trigger Delay = Sample Rate x Required Delay (in seconds) or Desired delay / Image interval (reciprocal of sample rate).
    - I (Image Range slider) - requires (T) Trigger slide set to the left, and is used to add a delay between trigger detection and the stored image range by overwriting the memory buffer until the user-specified number of frames has been satisfied. This Trigger delay is represented with a white bar.
    - Duration - (determined by 'Sample Rate' and memory size) indicates the time it takes to fill the camera RAM (memory buffer).
    - Frames - indicates the number of pictures (frames / images) the camera RAM can store (determined by 'Resolution' and memory size).
    - Delay - indicates the trigger delay time in seconds. (a (-) minus number represent the 'Pre-trigger' frames recording duration).
    
   ![](../assets/Pasted_image_20230925130744.png)
    
- Capture/trigger: Capture button set the camera to start recording frames into the RAM memory into an endless loop until the camera receives the trigger signal. Once you press this button, the label will change to Abort Recording, useful to stop the acquisition. Also the Trigger button will be enabled, allowing you to manually trigger the camera
    

### Short settings descriptions and acquisition instructions

1. Select the desired camera in the list
2. Set the resolution: for INPA the maximum and 800x600 for FILD1
3. Set the desired sample rate
4. Write 999999999999 in the Exposure time box and click enter. Doing this, the program will set the maximum time compatible with the sample rate
5. Check that EDR is set to 0
6. In the ‘Last’ box of the _Image Range and Trigger Position_ section, write 10 * `<Your sample rate>`. This is the number of frames you will record after the trigger. The 10 is to record 10 seconds
7.  Wait until the timer is launch, when that happens click on CSR to subtract the dark noise
8. Click on the red big button ‘Capture’


> [!warning]
> Temperature in INPA camera can change a couple of degrees between shots, so it is useful to do the CSR before each shot. For FILD1 this is not needed and you can live with just one CSR at the beginning of the day


> [!info]
> The cameras are physically connected to a TS06 signal. Just click the Capture button and the TS06 will start the recording when needed.


### File saving

To save the video you need to go to the ‘Play’ Tab in the right menu and click in the button at the bottom

![](../assets/Pasted_image_20230925131048.png)

A menu like this will pop-up:
![[../assets/Pasted_image_20230925131107.png|Saving menu]]
- Select the `D://` drive as the destination folder
    
- Set the name to:  
```bash
 <#shot>_<name>.cin
```
    
  The default company name is .cine, but long ago when the phantom operation started in AUG was decided to use .cin and the extension is hardcored here and there in AUGtv, so better keep the convention .cin (There is the idea of removing this and use .cine as default, will come in the future) In any case, the file is 100% identical, no matter the name
    
   The `<name>` field is just 9404 for FILD1 and 24167 for INPA. if you write `<name>` as shown in the example above, the program will catch automatically the name from the camera and you avoid the possible error to make a mess if you are operating 2 cameras at the same time
    
- Select ‘User defined’ as range option. Set 0 as the initial frame and the last recorder frame as last frame, in this way you will only save frames from 0 (the trigger) to the end of the video and you will avoid saving 144Gb of video
    
- Be sure the 16 bpp option is marked (to save the video in the fulldynamic range). Notice taht the camera records in 12 bits, if you un-click this option, the measured will be scaled to 8 bits
    
- Click save and wait some minutes for the file to be saved
    

It can happen that when saving you get an error saying that a package was lost (error code -305, picture below), in those case, the saved file is just around 30 MB (just the header of the file and the timebase) no actual frame is saved. In general this is not an issue, as the frames are still in the RAM memory, of the camera. Just click ‘retry’ and the file should be properly saved.
![](./Figures/Pasted_image_20230925131332.png|Network Error in the phantom)


### Moving files

At the end of the day, move all your files to the folder ‘a_tosent’ and run the python script ‘flush_files_to_gpfs.py’ (double click on the file is enough) Note: the name may be wrong, but I have no access to the computer to check this, will be checked in a future version of this manual

This python script will copy the files to gpfs, check its integrity to be sure the file was not corrupted when sending it and after it, will move it to the delete folder, which is periodically cleaned to keep the computer free of old and copied files