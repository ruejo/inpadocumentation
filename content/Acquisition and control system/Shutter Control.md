---
title: Shutter control 
draft: false
Category: Index
---
## Cable connection

The shutter coil and switches are connected to the port B53 in the INPA port

The power supply and electronics which control the shutter and record the voltages are located in the bunker

The computer which control the shutter is located at the end of the control room

The power supply only gives voltage with one polarization (to open). There is a gray cable near the power supply which invert the polarization of the coil. Connect it between the coil and the power supply if you want the coil to push in the direction of closing the shutter (just in case you want to help the spring to close it)

## Operation and shot file header modification

### Idea and run line

There are three phases of ‘operation’ during the shot:

1. Preparation: this phase starts when the shot number is released, usually some minutes before the shot
2. Acquisition: from shortly before the shot
3. Control: after the shot

Usually some diagnostics receive voltage on the preparation phase, but if the shutter current cannot be maintained for too long because it will means that the coil is heated and put in risk. For that, the power supply has a hardware limitation of 20 s of operation. So during the preparation and control phases the voltage of the coil current will be zero. In the current set-up

> ⏱️ TS02→ Init power supply TS04 → Send Voltage to coil TS06 → Set time origin for the timebase

### Voltage values

|Campaign|Value [V]|
|---|---|
|2020-2021|2|
|2021-2022 March|2|
|2021-2022 June|2.5 (open switch get better pressed at 2.5T)|

### Connection to the pc

The adquisition and control PC is the sxd7, located at the control room 1 (just behind FILD rack). To connect to this computer one can run ssh to the pc

```bash
ssh sxd7
```

or use the alias:

```bash
ssh npi
```

### Folders and path

Once logged in, you need to go to the folder where the program which control the acquisition is stored:

```bash
cd /shotfiles/DIAGS/NPI
```

In that folder we have the following files

- NPI: The compiled executable file. The program which call all the execution and adquisition steps.
- NPI00000.sfh: The ShotFileHeader file, containing all the information for the program NPI to run
- NPI00000.sfh_*** : Backup of the ShotFileHeader (created when the shf is modified)
- ncessary_triggers.txt: A help file written by Philip, it contains the lines which should be executed to emulate the triggers, just in case you want to perform a test. Basically:

```bash
/usr/ads/etc/MCTriggerSend MCPBTS02 ; sleep 1 ; /usr/ads/etc/MCTriggerSend MCPBTS04 ; sleep 4 ; /usr/ads/etc/MCTriggerSend MCCTTS06
```

### Edit the ShotFile Header

In order to edit the shotfile header, there is a program called xsfed. To call it to edit the file NPI00000.sfh:

```bash
xsfed NPI00000.sfh
```

The following windows will pop-up:
![[../assets/Pasted_image_20230922150716.png]]
There you can control each step of the process. The only point one need to touch is the `UShutter`. The rest of the fields are out of the scope of this manual, and if you want to modify them, please contact the CODAC team.

If you click in `Ushutter` the following window with the overview will pop-up:
![[../assets/Pasted_image_20230922150830.png]]

click on `nextpage` and you will see:
![[../assets/Pasted_image_20230922150914.png]]
Each row control the voltage of each output in each phase. Notice that only the output 0 is used for the power supply of the shutter, so this is the only entrance you should modify. As said before, the Shutter is just polarised in the acquire phase, so only the Adquire column of the first row needs to be modify.

> 💡 **Nota bene**: The program is a bit particular, you need to click in the number you need to modify and then keep the mouse on top of the number. If you move the mouse, the focus is lost and you can no longer write no it.



Once you are done, click done to exit these windows and click save in the first one. You will get some minor errors telling you that there is no calibration parameters for some of the signals (like the voltage of the switches) you can safely ignore them.
### Turn on the system

As of July 2022, the system is not routinely operated in every shot so you have to activate the program yourself. For that, first of all your user need to have rights for the operation → contact CODAC team. Once you have it:

- To set the program to write private shot files (for testing) 
    ```bash
    PRIV NPI    # Set the configuration for private
    NPI         # Start the measurement process
    ```
- To set it running in the
    ```bash
    AUGD NPI    # Set the configuration for public
    NPI         # Start the measurement process
    ```
