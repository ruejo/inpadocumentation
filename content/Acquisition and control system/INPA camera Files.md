---
title: INPA camera files
draft: false
Category: Data Storage
---
## PCO camera
Raw data from the PCO camera is installed in

```bash
/shares/experiments/aug-rawfiles/INP
```

There is a file for each 10 shot, ie in the folder ./4025 there are all shots between 40250 and 40259. These folders have a maximum volume of 4Gb (Double check this, the 4Gb limit was during AFS times,maybe in the shares they changed), which should be more than enough for the expected volume ($\sim 350$ Mb per shot)

> [!NOTE]
>  In case of need of more quota for each folder, contact AUG IT support via [augsupport@ipp.mpg.de](mailto:augsupport@ipp.mpg.de), or, during experiments, just ask to the IT responsible in the front row of the control room

The data is stored as a series of uncompressed PNG/TIFF files, one per frame. The camera parameters are stored in a namelist in a `.txt` in the folder


## Phantom camera
The raw data from the Phantom camera, is located at the gpfs folder from the phantom camera videos from AUGTV:
```bash
/p/IPP/AUG/rawfiles/FIT/
```


> [!important] No longer accesible
> After the shares transition, this folder was not moved by the IT and gpfs is no longer automounted in toki, so the only way of accesing the files, for now, is logging into lxts and from there cd to /gpfs/aug/rawfiles/FIT . In the long term the files will be at: /shares/experiments/aug-rawfiles/FIT

The data is stored as a `.cin` file. One file per shot. The file name ends with the camera serial number: 9404 for FILD1 camera and 24167 for the original INPA camera


## Reading the data
In order to read the data, the use of the [ScintSuite](https://github.com/JoseRuedaRueda/ScintSuite)  is recommended. Please see its documentation and the examples therein for full information.