---
title: Acquisition and control system
draft: false
Category: Index
---
The INPA shutter control is described here
- [Shutter Control](Shutter%20Control.md)

The INPA has two acquisition systems:
- [Photo-multipliers channel](Photo-multipliers%20channel.md): fast (tens or hundreds of kHz) but low resolution in phase space
- [Phantom camera](Phantom%20camera.md): 'slow' ($\le 1$ kHz, as there is not enough light to go faster) but high resolution in phase space

The information on where the camera files are installed can be found here:
- [INPA camera Files](INPA%20camera%20Files.md)


