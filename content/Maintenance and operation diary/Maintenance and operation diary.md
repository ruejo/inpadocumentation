
#### 2024-06-21
Summary of the work done this past to weeks in INPA reinstallation.

Carbon foils: one is ok and another is slightly wrinkled. The small ones are ok.

We found a lot of damaged screws (some of them broken). This meant we had to remove
the head from inside the vessel again. 
- Screws holding the optical plate were severely damaged. By recommendation of
M. Ebner changed them for two M5 screws. Still good optical alignement.
- Changed the majority of the screws of the head for new ones.
- Changed the screws that fix the support to the vessel (they were too long).
- _You can remove and install the head using the four screws in front instead of
doing the original tricky thing. They are not welded!_
- Cables inside were giving us shortcuts. Now they are fixed to the side of the
port with two clips.
- Lamp is working.

![Two new screws in the optic plate](./assets/IMG20240618173721.jpg)

There has been a change in the cables, and now the port output is:
- B51: shutter
- B52: magnetic coils
- B53: lamp

Next work:
- Carbon foil needs to be installed in the calibration week. Optical alignement
will be checked that week.
- Out-vessel optic tube needs a support to keep it in place.

#### 2024-08-23
Summary of the work done in the calibrations.

We have checked the optical alignment and focus of the camera. Did the absolute calibration of the Phantom.

![Close view of the integrating sphere and the INPA head](./assets/IMG20240816103642.jpg)

![Set up for the absolute calibration. It was a bit sketchy, but it's more stable than it looks](./assets/IMG20240816104009.jpg)

Also took a distortion frame.

![Distorion frame](./assets/distortion2.tif)

![Checkered pattern used in for the distortion](./assets/IMG20240816123636.jpg)

A new scintillator has been installed. The carbon foil was installed without
any problem. The scintillator is marked every 2 cm (very precisely). The red arrows
show the starting point of the marks.

![New marked scintillator](./assets/IMG_20240822_103103.jpg)

The camera resolution is set to 1280x800, which I think it limits the number of
frames we can store for each shot (video of shorter lenght). So, to improve the 
duration of the videos we should decrease the fps. Either way, this camera is not
fast enough to see fluctuations, so we are not really loosing a lot.

To be done:
- PMT comissioning: get the PMT ready and place the optic fibers correctly.
- Phantom cables: we got some new cables for the trigger and ethernet. Make 
sure they are working correctly
- Close camera shielding box and cover optics the optics below it.