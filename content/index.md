---
title: Documentation of the INPA diagnostic
draft: false
Category: Index
---
This is the documentation of the INPA diagnostic. Here you will find the notes on the design, installation and operation of the INPA diagnostic. You will not find any references to the data analysis codes. You can find those codes and the examples of how to run it in their repos:

- [ScintSuite](https://github.com/JoseRuedaRueda/ScintSuite): To analyse experimental data
- [SINPA (uFILDSIM) ](https://github.com/JoseRuedaRueda/uFILDSIM): To calculate strike maps and synthetic signals (given an input in the pinhole)
- [FIDASIM4](https://gitlab.mpcdf.mpg.de/akappato/FIDASIM4): To calculate the CX flux at the detector pinhole 

This guide contains:
- [[Design of the diagnostic/Design of the diagnostic]]
- [[Installation of the diagnostic]]
- [[Acquisition and control system/Acquisition and control system]]
- [[Maintenance and operation diary/Maintenance and operation diary]]
- [[Instruction to update the guide/Instruction to update the guide]]