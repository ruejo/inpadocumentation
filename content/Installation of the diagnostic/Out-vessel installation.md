# Overview

The INPA out-vessel components are, roughly speaking, a lens to focus the image, a beam splitter, a camera and the PMTs.

In February 2022, the beam splitter and the PMTs were not installed, just an old PCO camera was installed to start the commissioning

# Installation of optical elements

### Step 0: Preparing the elements

1. Insert the lenses and the mirror in the support structures. Note that a kapton ring must be inserted between the support and the lenses and mirrors. These kapton foils will avoid direct contact of the fixation system with the lens and distribute a bit more homogeneously the pressure. These rings are not in the CAD design
2. The doublet support (DTI 163) needed to be modified a bit because the doublet model was changed after designing the support and the new one has a bigger diameter (optically speaking it has the same focal length, curvature and so on, the only difference is the diameter of the lens). After the modification, the lens fit in the support but is very tight. You need to be extremely careful inserting and extracting the doublet (it comes from USA, so it is a bit painful to order one new if this is broken, and it cost 400$ and take almost 2 month in being manufacture, don’t break it!!)
3. It can be difficult to identify which is the mirror face of the beam splitter. The mirror face is marked with a small dot in the corner, but some doubt appear you can always check it quickly, as Edmund Optics customer service explained: *Let the beam splitter rest in a table, on top of some clean paper. Approach a sharp object (such a pencil) to the corner of one surface. Look at the beam splitter tangentially, if the reflected image is touching the pencil, that face is the mirror face. If there is a gap between the reflection and the object, that face was not the mirror one*

> [!important]  
> Insert the kapton foils in the holders  
  
> [!important]  
> The support for the doublet is really tight, be careful inserting this piece  

### Step 1: Tube and optical rails

**Tube**

1. The black tube must be connected in the flange with the piece designed to that end. Plastic screws are used. This was an advice from Wolfgang, as the join is already tight enough and the weight of the optical elements is small, so there is no need of using steel screws which could deform the tube as high pressure can be applied without noticing
2. Set the tube horizontal, with the help of the mechanical table. This is crucial because if the tube is not horizontal, then the mirror will not deflect the light to the vertical and there would not be signal in the phantom (as the mirror will not form 45 degrees with the tube installed inside the vessel
    
    ![](../assets/IMG_0408.jpg)
    Fig1: View of the first instalation of the INPA optical tube
    
    ![](../assets/IMG_0449.jpg)
    Fig 2: Example of comprobation that the tube is horizontal
    
    ![](../assets/photo_2022-02-17_16-02-03.jpg)
    Fig 3: Squeme of the issues which appear if the tube inside is not aligned with the one outside, the image could miss the camera sensor
    

> [!important]  
> Make sure the tube is horizontal  

**Optical rails**

1. Install the two fixation pieces in the tube (DTI 161)
2. Make sure that the upper face of the two fixation pieces are horizontal (see picture below). If this is not the case, the image will not be defelected in to the vertical direction, so there would not be image in the camera
3. Insert the rails such that the rails end is directly at the end of the support piece so there is not part of the rail outside. Note: the CAD design leaves 5 cm of margin, but the platform is large enough, so there is no need of leaving such a margin and having a bit longer rail will come handy in the future to focus the PMT.

> [!important]  
> Make sure the fixation pieces are horizontal  

### Step 2: Mirror and lens

1. Place the mirror and lens as said by CAD (or take advantage of the optimal position found by me, detailed in the drawing at the end of the document). Do not tight the screws to fix the position of the mirror.
2. Place the phantom in the approximate position. If the shielding box structure is not mounted and you can’t place the camera in the ‘vertical direction’ turn 90 degrees the mirror to make the set up horizontal (this is only valid for focusing purpose, do not try to operate like this!). If this is the case, you will need to put something below the phantom to elevate it a bit. To packages of 500 paper sheets are more or less perfect height.
3. Turn on the calibration lamp. Notice that if the lamp is broken, game over, you can not put the camera in focus neither check nothing during the campaign. Saving the lamp is of capital importance. The lamp is of 10W, but at 2.5W (around 5 V) already emit enough light to carry on the alignment so do not overpass this value. Use a multimiter to monitor the current in the lamp. Do not overpass the 0.5A
4. Play a bit with the position of the camera and the lens until you find a nice image. The best reference is the lower edge of the scintillator. Try to do not move the mirror, as if the mirror is not centered with the aperture in the shielding box you will not see anything
5. Fix the mirror and lens position. Warning, the holders are in aluminum, it is really easy to do too much pressure with a steel screw and damage the threaded holes. Be gentle (I say this because I already destroyed one hole of the mirror holder, ups)

> [!important]  
> Do not pass 2.5 W (0.5A) on the lamp!!!  

### Step 3a: PCO positioning

If you are installing the PCO camera without the PMTs, as done in the first stage of the commissioning:

1. Remove the beam splitter as it is not needed for this set-up
2. Join the camera to the piece (DTI 165) but do not tight the screws, as you will need some freedom
3. Place the support more or less at the CAD (or the drawing below) position. Turn on the calibration lamp and with paicience look for the focus point.
4. Notice that the image is larger than the sensor of this camera so you will not be able to see it completely. You must move along the rail, for each rail position, you should move the camera a bit up and down along the support, to find the edge of the image (scintillator) and see if this is focused
5. Once found, tight everything, again taking care of the screws and the aluminum, place the lenses cover and block the possible and put a bit of tape in the few joints which are not blocked by the cover

> [!important]  
> This step of focusing in the PCO camera is long, painful and seems hopeless, but there is light at the end of the road, the sweet spot is there  

### Step 3b: Phantom positioning

If you did Step 2 with the camera in the shielding box your work is done, just place the small tube (DTI 160) to be sure of blocking all the light, close the cover, close the shielding box, put a bit of tape in the spot were light could enter and enjoy.

If you place the phantom for the first time in the shielding box and you do not get any signal, you can play a bit with the mechanical table to tilt a bit the system, to align perfectly the outvessel tube with the invessel tube (see figure 3)

# Status and limits

### 02/2022: Preliminary installation

**Resolution and focus in the phantom**

The image on the phantom camera can be seen below. It looks quite well defined and the FoV seems the same as during the calibration, which is nice, no movement nor misalignment were produced during baking or fist disruptions of the campaign

**Ghost**

There is a ghost in the image, this ghost came from reflection in the outer metal tube. Some bafle plates will be installed to avoid this (more provable just 3D printed plastic would be enough, they are designed, just need to send them to print)

**Changes in the out-vessel geometry**

The original plan was to install the PCO camera in a 3D printed support, a nice tower. This tower was manufactured but at the end it was not used because:

- the PMTs were not ready and using the towers meant to use the beam splitter so lost 50% of the signal for nothing
- it was really difficult to align the PCO camera due to the fact that the image is larger than the sensor, there was no so much freedom.

improvised a bit a solution using some of the 3D printed pieces of the tower and using the support form the PMTs

**Resolution and focus in the PCO**

The image on the PCO camera can be seen below for a couple of camera positions. Notice that there is a lot of noise, this is an old PCO camera and it is a bit in the end of its working life

**Selection of the camera position**

In this first installation of the commissioning, I installed the PCO such that the corner of the scintillator closer to the carbon foil is seen. This should correspond to lower energies and passive signal, I expect to always see something there

**Cabling installation**

We reuse the data converter and acquisition system of FILD5. To this end, the data converter of FILD5 PCO was installed at sector 16 (see image below) and fibers were installed from sector 16 to sector 8, as we can not use a network switch because the camera use some connection protocols which are not switchable

The connection scheme is the following:

1. Ethernet from lnxcamfiild2 computer (rack 77 of CR) to data converter in rack 77 of CR)
2. Fibers from that point to cabinet near MEM (fiber number...)
3. Fibers from cabinet near MEM to sector 16 below SPI
4. Small 2m fiber up to the installation point of the second data converter
5. 4m Ethernet from the data converter to the camera

Notice that the 2m fibers could have been avoided, but there was a communication issue between the technicians and me (entirely my fault) so we needed to put this little extension. The cables are just used to transmit the signal and there is zero issues of having this connection in the optical fiber part

Roland Merkel tried to put an Ethernet insulator, but it did not work, as this Ethernet has a internal optical conversion which seems not to be compatible with the PCO (the camera needs to be powered via the Ethernet so blocking the power transmission makes the camera not to work)

> [!important]  
> Ethernet insulator can’t be installed because camera needs to be powered via Ethernet. So the Ethernet must be kept as short as possible  

**Camera power**

As this installation was temporally, there is no dedicated powfit installed, the camera must be switched on and of manually via the small stick in the dataconverter installed in the torus hall

> [!important]  
> The camera must be switched on/off manually in the torus hall  

![](assets/IMG_0426.jpg)
Fig 4: Location of the data converter, just in the corner of the SPI tank (for the PCO camera only)

![](assets/IMG_0428.jpg)
Fig 5: detail of the lever which need to be used to power on the camera (for the PCO camera only)

  

# Positioning drawings

[Drawing PDF with coordinates for phantom](../assets/DTI_161.pdf)



[Drawing PDF with coordinates for PĈO](../assets/Documentos_escaneados_(2).pdf)

In both cases, the black tube ends on the second DTI 161, as in the case of the previous drawing

![](assets/80E23541-EA37-4106-9961-DAC724CBC675.jpeg)
No time to make a drawing of this, just leave this two pictures temporaly. Notice the yellow tape to ensure the objective is not moved

![](assets/C888C7E9-9D0F-4379-B2CF-B86CB0093670.jpeg)


