---
title: Installation of the diagnostic
draft: false
Category: Installation
---
This contains brief notes of how to install the INPA diagnostic. It is composed of two documents:

- [In-vessel installation](In-vessel%20installation.md)
- [Out-vessel installation](Out-vessel%20installation.md)