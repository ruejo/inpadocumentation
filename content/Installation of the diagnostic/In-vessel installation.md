This guide summarizes the required steps to install the in-vessel components of the INPA diagnostic
# Pre-requisites:
This guide assume that you have prepare the system as detailed in the manual: [[Laboratory assembly]]


# Stop 0: Lab check
First step is going to the lab were you have the INPA pieces and introduce in different plastic bags each piece and the set of screws, in order to easily take the pieces you need to inside the vessel. This step is fundamental and maybe the most important. If not, you will lost many screws along the way and you will need to enter and going out of the vessel each time.

![](../assets/IMG_20210820_154619.jpg)

# Step 1: Long tube installation
The first step is the installation of the long tube. This tube is assembled to the vacuum window. It has two fixation points: 
1. Flank (outside vessel), standard connection
2. 'C' support, just near the periscope

![Tube + Periscope going out of the lab, in its way to the vessel](../assets/IMG_20210823_091941.jpg)

Please label the ends of the cable, such you can latter identify them, this can be done just with a bit o kapton tape. 

Although this seems like the most easy and trivial point, is actually the most difficult one. Because you need the tube to be the most horizontal as possible. The tube has to be horizontal, so no torsion force is applied in the flank and the copper ring which seals the window can be equally fixated. If the tube is not perfectly horizontal, the copper won't be symmetrically placed so there would be air leaks. This happened during the first installation of the diagnostic

Placing it horizontal was is achieved thanks to a support structure with C shape installed inside the port, but is in the limit of what your arm can reach and the tube is quite heavy. We could do the work during the installation using some adjustable platforms, but due to the several cables and so on which are present in the port, even this was ridiculously complicated, as the platforms turned when we were doing pressure. At the end it was needed brute human force to elevate the in-vessel end of the tube. Notice that you can not make a lot of force in the periscope end, because you could move the periscope and destroy the optical alignment. If this step needs to be re-done in the future, please just 3D print some dummy plastic support with the port curvature such that there is a stable base to make force with the adjustable platforms.

![](../assets/IMG_20210824_122107.jpg)

> [!success] New method
> Instead of pushing by hand, we now have an air cushion which will make the work for us. This should be not only more comfortable, but safer, as homogeneous pressure will be done to the periscope  

Once the tube is fixated in the port, you need to fixated the piece 25-C to the vessel port, in the two bolts which are welded to the vessel. Notice that this piece was modified respect to the initial design (shown in the drawing below) in order to be able to tight it from the front, which is needed because in this installation the space will be more limited.

![Drawing of the piece 25-C](../assets/image-20240123155022435.png)




![Bonus track. The author of this guide waiting. Picture taken just after discovering the air leak and knowing that we should start again. Yes, it was post pandemic and I need a hair cut](../assets/IMG_20210825_141419%201.jpg)


# Step 2: Periscope 1st installation
The initial idea was to use some tools to move the wedge. But honestly this was just impossible. It is too hard and it is impossible (there is too much friction, it was a compromise between being able to move it and don’t let it too loosy so it move during operation). So, the idea is to move it with the hand, like inserting a screw. This is nice, but surprise, is that not turn because it collide with the port

The idea is dismount 2 of the 4 faces of the periscope, screw it comfortably until the desired height, turn it a bit and mount the removed faces. Seems complicated, but is actually quite, nice and smooth (this was the nice surprise of the installation. Just left always the curve face, which is screwed from the bottom and is more complicated screw from far

Once done, just screw the periscope lower tube. Take a 1cm wide plastic piece (you can find many of them in Wolfgang Popken lab in L5) and be sure it can enter between the periscope and the head. This will be your safety check than the safety margins are fulfilled.

# Step 3: Head support and head alignment

If this is not the first INPA installation, you can skip this point and go to point 4. Long story short, in this stage you will put the head in position and drill the hole where you will insert the screw to fix permanently the head position and orientation.

Now you need to fix the head support to the wall. This is done with some bolts fixed by the vessel crew.

At this point, you should have something like this:

![](../assets/IMG_20210827_085534.jpg)

Status after the first sub-step of block 4

Once done, place the head in position. Notice that at this point the head should not have any walls. To have easier access. Also notice that at this point, the bolometer of sector 16 should be dismounted. To be able to have easier access to the support.

Notice here that the B coil is 2-3 mm higher than expected from CAD, so the plastic piece which was designed to hold the head is not useful. But worry not, a single small clamp is enough to fix the head well enough while doing this

Now is the patience point, you need to install a rope, to mimic the NBI3 position, this is done more or less with the CAD position. Notice that the INPA has 20cm of FWHM, is not the end of the world is there is a couple of mm of dis-alignment.

![](../assets/IMG_20210830_173829.jpg)

Rope installation. This wire which looks randomly place was placed with a lot of work and help of Wolfgang Zeidner. : )

Now you need to place the lasers in the INPA collimator. There is a 3D printed support for this. In the lab, place the lasers in the support and try them to be as ‘horizontal’ as possible, just place it on the table and see their strike positions some m away. In the piece we used during the installation, we glued the laser diodes for them not to move. You can use this piece. Note, one of the extreme diodes (the one on the LFS) is a bit tilted, we culd not do anything better, but is still good enough for the alignement

Now you have to use the nice and calibrated carboard (we basically stick a squared paper in to a cardboard and make some lines, it should be still in wolfgang Lab if you needed), turn on the lasers, place the cardboard on the rope and see how far the lasers ends. Tip, fix the battery holder for the lasers to the head structure with a plastic rope, so it not in the middle of the area you will need to touch while moving the head.

![](../assets/IMG_20210830_173917.jpg)
Figure: Example of the ‘high precision’ calibrated cardboard being used

![](../assets/IMG_20210830_174249.jpg)
Figure: Head with the lasers, notices the black plastic rope fixing the batteries on place. Notice also that a small sticker was placed in order to keep the shutter opened.

After playing a bit with the position of the head, you should get something like this, the laser within 1 cm from the rope:
![](../assets/IMG_20210830_173858.jpg)
(to achieve this position and resect the clearance for the graphite tails, we needed to shorten a bit the periscope tube. This had no mayor implications in the optics, I checked it on Zeemax, also we needed to biselate a bit the edge of the box, this was due to a bad design of the graphite tails)

You also need to be sure, using the camera outvessel, that the scintillator is on focus!

Now you need to mark the position to drill the hole. This is done inserting a special pin and hitting it with a small hammer (the vessel crew has this beautifull idea, they took a pin from the hole diameter and sharp one of the ends such that we have a perfect marking tool for the center, unfortunately I have no pictures of the pin). You can use one fo the mirror ‘dentist’ like to see that the mark is actually done

Now is the moment to take it to the workshop, they will drill the hole.

After this hole is drilled, you place the big screw, in the workshop and have something like this

![](../assets/IMG_20210907_172044.jpg)

Do not tight it to the maximum!!!

Go in vessel, place the head again, turn on the lasers and rotate the head until the alignment is good again. Once you have it, you mark the position for the 2 M8 screws. For this, you need no pin, because there is a tool you can insert and turn a bit such that it adapts to the M8 Hole and it incorporates a pin for the mark.

Now, to the workshop again!

Once you have it back, it should be cleaned and baked and until is 100% cold (if you put the screw once it is hot and then it could down, it can be damaged due to tensions after volume changing, yes, you may be thinking that it is just 0.01mm, but all technicians told me it could be damaged soooo, you need patience) Is more than a kg of metal so the cooling down is slower than one could think and you can not water cool it… so you need a lot of patience. You can wait in the coffee room

![](../assets/IMG_20210823_093002.jpg)

(Actually this is not a picture of me waiting at this point, it is of me waiting before starting with the tube)
# Step 4: Head support and cabling
At this point you are ready to close the head walls and place the head in position.

This is also the funny step where you realize that the cables are just in the port and you have to insert deeply the hand on it to gather them. Worry not, they are there, they are welded to the tube so there is no chance you have lost them.

Take the optical platform, pass the cables though the lamp holder and connect them in the hole below the platform.

Take a lot of care of not clamping the cable with the optical platform.

Once they are connected, there are some small metal sheet welded to the INPA head though to route the cable outside the field of view.

# Step 5: Install the carbon foil
 This needs to be done in the calibration week, right before the vessel is closed and wear a face mask to avoid air currents:
- 1 Build a tower with the blue boxes such that you have a short distance between the INPA head and your workplace
- 2 open the foil package, really carefully, UNTIGHT the screw, if you turn the screw in the wrong direction, all is destroyed
- 3 take it with extreme care place it in position and screw the fixation support
- 4 move the plate to the position and screw. For the movement, do it cm per cm do not rush!!!

