---
title: Instructions to Update the guide
draft: false
Category: Instructions
---

The guide is written in MarkDown (md) format, you can check the general commands and instruction for MarkDown [here](https://daringfireball.net/projects/markdown/). If you want to edit it, an editor such as VSCode or [Obsidian](https://obsidian.md/) are highly recomemended.

The program which read the .md files and create the html for the documentation is [Quartz](https://quartz.jzhao.xyz/). Actually, the repo below this page is a clone of quartz.

To edit the documentation:

- 1 Clone the repo
```bash
git clone https://gitlab.mpcdf.mpg.de/ruejo/inpadocumentation/
```
- 2 Modify with your favorite text program the .md files (please note that the first page to display would be the index and pages which are not references there of by any intermediate page won't be easy to access)
- 3 Commit your changes and push (I assume you cloned the repo in your home folder, change the `cd` line accordingly).
```bash
cd ~/inpadocumentation
git add *
git commit -m 'Updated Documentation v x.x.x'
git push
```

Once it is pushed, a pipeline will be executed and the documentation will be compiled. It can take a few minutes to enter the MPCDF GitLab queue. Do not panic if the changes are not seen just after uploading to the repo.


